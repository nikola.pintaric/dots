#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1='[\u@\h \W]\$ '



source ~/.aliases

export PATH=$PATH:$HOME/.scripts/
export EDITOR=vim

[ -f ~/.fzf.bash ] && source ~/.fzf.bash
fd() {
  local dir
  dir=$(find ${1:-.} -path '*/\.*' -prune \
                  -o -type d -print 2> /dev/null | fzf +m) &&
  cd "$dir"
}


settitle() {
    printf "\033k$1\033\\"
}

ssh() {
    settitle "$*"
    command ssh "$@"
    settitle "bash"
}

#bind 'set editing-mode vi'
#bind 'set show-mode-in-prompt on'
#bind 'set vi-ins-mode-string INSERT'
#bind 'set vi-cmd-mode-string NORMAL'

export PKG_CONFIG_PATH="/usr/local/lib/pkgconfig"
export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"
export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"

#export GDK_BACKEND=wayland
#export CLUTTER_BACKEND=wayland
#export XDG_RUNTIME_DIR='/var/run/chrome'
#export WAYLAND_DISPLAY=wayland-0
#export DISPLAY=:0
