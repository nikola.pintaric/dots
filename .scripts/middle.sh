WID=$1
ROOT=$(lsw -r)

SW=$(wattr w $ROOT)
SH=$(wattr h $ROOT)

X=$(echo $SW/2 | bc)
Y=$(echo $SH/2 | bc)

echo $X $Y $WID
wmv -a $X $Y $WID
