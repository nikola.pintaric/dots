"        _
" __   _(_)_ __ ___  _ __ ___
" \ \ / / | '_ ` _ \| '__/ __|
"  \ V /| | | | | | | | | (__
"   \_/ |_|_| |_| |_|_|  \___|
" 

" Default settings {{{

set nu nocp nowrap mouse=a gd ml si ai
set sts=2 sw=2 ts=2 bri ai et
set sb spr ss=1 siso=5 ttymouse=xterm2
set ut=100

filetype plugin indent on
syntax on

" }}}

" Plugins {{{

let g:closetag_filenames = "*.html,*.xhtml,*.phtml,*.erb,*.jsx,*.tsx"

runtime ftplugin/man.vim

" inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
" inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
" inoremap <silent><expr> <c-space> coc#refresh()

" }}}

" Mappings {{{

let mapleader=' '

nnoremap : ;
nnoremap ; :
vnoremap : ;
vnoremap ; :

xnoremap P "_dP

nnoremap <leader>t :tabe<cr>,
nnoremap <leader>w :w<cr>,
nnoremap <leader>q :q<cr>,
nnoremap <leader>r q

nnoremap <leader>s :s/
nnoremap <leader>S :%s/
vnoremap <leader>s :s/
vnoremap <leader>S :%s/

nnoremap <c-m> K
vnoremap <c-m> K

nnoremap - :NERDTree %:h/<cr>

nnoremap gp `[v`]
nnoremap <C-W><C-F> <C-W>vgf

nnoremap <silent> gb :silent! exec "!xdg-open \"https://duckduckgo.com/?q=" .  escape(EncodeURL(expand("<cWORD>")), '%') . "\" > /dev/null " <CR>
vnoremap <silent> gb "gy:silent! exec "!xdg-open \"https://duckduckgo.com/?q=".  escape(EncodeURL(getreg("g")), '%') . "\" " <CR>

nnoremap <leader>p :Clap files<cr>
nnoremap <leader>P :Clap git_files<cr>
nnoremap <leader>b :Clap buffers<cr>
nnoremap <leader>l :Clap grep<cr>

nnoremap <leader>g :Gstatus<cr>

nnoremap <leader>y :GitGutterPrevHunk<cr>
nnoremap <leader>o :GitGutterNextHunk<cr>

nnoremap <leader>f :NERDTreeFind<cr>
nnoremap <c-n> :NERDTreeToggle<cr>

command! W :execute ':silent w !sudo tee % > /dev/null' | :edit!
cabbrev h vert h

" workman keymap {{{
let g:keymaps =  [
			\  {
			\    'name': 'workman',
			\    'keymap': {
			\      'noremap': {
			\        'h': 'y',
			\        'j': 'n',
			\        'k': 'e',
			\        'l': 'o',
			\        'y': 'h',
			\        'n': 'j',
			\        'e': 'k',
			\        'o': 'l',
			\        'H': 'y$',
			\        'J': 'N',
			\        'K': 'E',
			\        'L': 'O',
			\        'Y': 'H',
			\        'N': '5j',
			\        'E': '5k',
			\        'O': 'L',
      \        '<leader>n': 'gT',
      \        '<leader>e': 'gt',
      \        '<leader>N': ':tabm-<cr>',
      \        '<leader>E': ':tabm+<cr>',
			\				 '<c-w>y': '<c-w>h',
			\				 '<c-w>n': '<c-w>j',
			\				 '<c-w>e': '<c-w>k',
			\				 '<c-w>o': '<c-w>l',
			\				 '<c-w>Y': '<c-w>H',
			\				 '<c-w>N': '<c-w>J',
			\				 '<c-w>E': '<c-w>K',
			\				 '<c-w>O': '<c-w>L',
			\				 '<c-w>l': '<c-w>o',
			\				 'q': 'ok',
			\				 'Q': 'Oj',
			\      },
			\    },
			\  },]
" }}}

" }}}

" Colors {{{

set background=dark

hi folded ctermbg=none ctermfg=2
hi MatchParen ctermbg=235

hi TabLineFill ctermbg=none
hi TabLineFill ctermbg=none

hi pmenu ctermbg=233

" hi clear SignColumn

" hi SignColumn ctermbg=none

" hi MatchParen ctermbg=8

"colorscheme wal

" hi clear TabLine
" hi clear TabLineFill
" hi clear TabLineSel
" hi clear VertSplit
" hi TabLineSel ctermfg=1 cterm=bold

" hi clear pmenu
" hi pmenu ctermfg=7

" }}}

" UI {{{

let g:netrw_banner=0

" }}}


" vim:foldmethod=marker:foldlevel=0
