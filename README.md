alias config='/usr/bin/git --git-dir=$HOME/.dots/ --work-tree=$HOME'

echo ".dots" >> .gitignore

git clone --bare https://gitlab.com/nikola.pintaric/dots/ .dots

dots checkout